/*
mailtextbody - a program that returns the body of an email message

Copyright (C) 2005-2024 Toastfreeware <toast@toastfreeware.priv.at> - 
Philipp Spitzer and Gregor Herrmann

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

*/

#include <iostream>
#include <cstdlib>
#include <mimetic/mimetic.h>
#include <argp.h>

using namespace std;
using namespace mimetic;


const char *argp_program_version = "mailtextbody 0.1.5";
const char *argp_program_bug_address = "<toast@toastfreeware.priv.at>";
static char doc[] = "mailtextbody - a program that returns the body of an email message";
static char args_doc[] = "mailtextbody < rawmail > textplain";
static const struct argp_option argp_options[] = {
	{"mimetype", 'm', "value", 0, "MIME type to look for, default \"text/plain\".", 0},
	{0}
};


struct arguments
{
	const char* mime_type = "text/plain";
};


// parse function for the program options
static error_t parse_opt(int key, char* arg, argp_state* state) {
	struct arguments* arguments = (struct arguments*) state->input;
	switch (key) {
	case 'm':
		arguments->mime_type = arg;
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}


// argp parser.
static struct argp argp = {argp_options, parse_opt, args_doc, doc};


string uppercase(const string& s) {
	string r = s;
	for (string::size_type i = 0; i != r.size(); ++i) r[i] = toupper(r[i]);
	return r;
}


string decodeMimePart(Body::const_iterator begin, Body::const_iterator end, const string& encoding) {
	string result;
	if (uppercase(encoding) == "QUOTED-PRINTABLE") {QP::Decoder d; decode(begin, end, d, back_inserter(result));}
	else if (uppercase(encoding) == "BASE64") {Base64::Decoder d; decode(begin, end, d, back_inserter(result));}
	else {NullCodec d; decode(begin, end, d, back_inserter(result));}
	return result;
}


bool findMimePart(MimeEntity* pMe, const string& mimeType) {
	Header& header = pMe->header();
	if (uppercase(header.contentType().str()).find(uppercase(mimeType)) != string::npos) {
		cout << decodeMimePart(pMe->body().begin(), pMe->body().end(), pMe->header().contentTransferEncoding().str());
		return true;
	}
	MimeEntityList& parts = pMe->body().parts();
	// cycle on sub entities list and print info of every item
	for (MimeEntityList::iterator mbit = parts.begin(); mbit != parts.end(); ++mbit) {
		if (findMimePart(*mbit, mimeType)) return true;
	}
	return false;
}


int main(int argc, char *argv[]) {
	// parse command line options
	struct arguments arguments;
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	MimeEntity me(cin); // parse and load message
	if (!findMimePart(&me, arguments.mime_type)) {
		if (me.header().mimeVersion().str() == "0.0") cout << me.body();
	}
	return 0;
}
